#!/usr/bin/perl
#    Copyright (C) 2018 Man Kai KAM
#    This file is part of Tracemail.
#
#    Tracemail is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Tracemail is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Tracemail.  If not, see <http://www.gnu.org/licenses/>.

use IO::Handle;

$VERSION = '1.0.1';
use Cwd 'abs_path';
use File::Basename;
my $whereiam =dirname(abs_path( __FILE__));

use DBI;

my $N = 10;
$SIG{HUP} = \&reload;

my $f = '/var/log/maillog';
my $fh;
open($fh, '<', $f);

my $fwf = $whereiam . '/fw.rules';
my $fwfh;
open($fwfh, '>', $fwf);

sub reload {
  close($fh);
  open($fh, '<', $f);
}

sub writepid {
my $pidf = '/var/run/banhack.pid';
my $pidfh;
open($pidfh, '>', $pidf);
print $pidfh $$;
close($pidfh);

}

my $driver = "SQLite";
my $db_name = $whereiam . "/banhack.db";
my $dbd = "DBI:$driver:dbname=$db_name";
my $username = "";
my $password = "";

&writepid();

my $dbh = DBI->connect($dbd, $username, $password, { RaiseError => 1 })
                      or die $DBI::errstr;
my $writefw=0;
my $line;
while (1) {
  if (eof($fh)) {
    sleep 1;
    $fh->clearerr;
    next;
  }
  $line = <$fh>;
  my $sth;
  if ($line =~ /\[([0-9\.]+)\]: SASL LOGIN authentication failed/) {
    my $stmt = qq(select count from ban where ip = ?;);
    $sth = $dbh->prepare($stmt) or die "execute fail";
    if ($ret = $sth->execute($1)) {
      if (my @row = $sth->fetchrow_array()) {
        my $stmt =  qq(update ban set count = count + 1 where ip = '$1';);
        $dbh->do($stmt);
        if ($row[0] == $N) {
          $writefw=1;
        }
      }
      else {
        my $stmt = qq(insert into ban (ip, count) values ('$1', 1););
        $dbh->do($stmt); 
      }
    }
    $sth->finish;
    if ($writefw==1)  {
       my $rule =  "iptables -I INPUT -s $1 -j DROP";
       $writefw = FALSE;
       system($rule);
       print $fwfh "$rule\n";
    }
  }
}
close($fh);
close($fwfh);
exit(0);
