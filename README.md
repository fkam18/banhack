# banhack

banhack scans /var/log/maillog (postfix sasl authen) to detect dictionary trial logon and ban the IP from iptables perpectually 

## Getting Started

### Prerequisites
perl 5.16.3 on Centos 6
sqlite3 cli and perl

### Installing

fkam:20181217

To install and run:
mkdir -p /root/admin/banhack
git clone ...
./banhack.setup
nohup ./banhack.pl &

in /etc/logrotate.d/syslog
/var/log/cron
/var/log/maillog
/var/log/messages
/var/log/secure
/var/log/spooler
{
    missingok
    sharedscripts
    postrotate
        /bin/kill -HUP `cat /var/run/syslogd.pid 2> /dev/null` 2> /dev/null || true
        /bin/kill -HUP `cat /var/run/banhack.pid 2> /dev/null` 2> /dev/null || true
    endscript
}
~             

## Running the tests


### Break down into end to end tests



### And coding style tests


